package sg.gov.ntp.tn.icm.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.hamcrest.number.OrderingComparison.greaterThan;

import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import sg.gov.ntp.tn.icm.dao.StudentRepository;
import sg.gov.ntp.tn.icm.model.Classroom;
import sg.gov.ntp.tn.icm.model.Student;
import sg.gov.ntp.tn.icm.model.StudentVO;

public class StudentServiceTest {

	@Mock
	private StudentRepository repositoryMock;

	@InjectMocks
	private StudentService service;

	private StudentVO studentVo;
	private Student student;
	private Classroom classroom;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		studentVo = new StudentVO();
		student = new Student();
		classroom = new Classroom();

	}

	@Test
	public void testFindById() {

		/**
		 * Test Condition
		 */
		student.setId("267");
		student.setFname("jose");
		student.setLname("rizal");
		student.setGender("M");
		student.setAge(19);
		student.setClassroom(classroom);

		classroom.setClassroomId(1);
		classroom.setClassroomNo(11);
		classroom.setStudent(student);
		
		when(repositoryMock.findById("267")).thenReturn(student);

		/**
		 * Checks if mocked repository does what it's supposed to return given
		 * the condition above.
		 */
		Student expectedStudent = service.getStudent("267");
		List <Classroom> expectedClassroom = expectedStudent.getClassroom();

		assertThat(expectedStudent, is(notNullValue()));
		assertThat(expectedStudent, is(instanceOf(Student.class)));
		
		assertThat("267", is(equalTo(expectedStudent.getId())));
		assertThat("jose",  is(equalTo(expectedStudent.getFname())));
		assertEquals("rizal", expectedStudent.getLname());
		assertEquals(19, expectedStudent.getAge());
		assertEquals("M", expectedStudent.getGender());
		
		assertEquals(1, expectedClassroom.get(0).getClassroomId());
		assertEquals(11, expectedClassroom.get(0).getClassroomNo());
		assertEquals(expectedStudent, expectedClassroom.get(0).getStudent());
	}

	@Test
	public void testSaveStudent() {

		studentVo.setFirstName("hazel");
		studentVo.setLastName("cosina");
		studentVo.setGender("F");
		studentVo.setAge("50");
		studentVo.setClassroomNumber(1);
		service.saveStudent(studentVo);

		verify(repositoryMock).save(any(Student.class));
		verify(repositoryMock, times(1)).save(any(Student.class));
	}
	
}