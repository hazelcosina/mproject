package sg.gov.ntp.tn.icm.service;

import sg.gov.ntp.tn.icm.model.Student;
import sg.gov.ntp.tn.icm.model.StudentVO;


public interface StudentFacade {

    public void saveStudent(StudentVO student);
    public Student getStudent(String id);
}
