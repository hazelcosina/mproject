package sg.gov.ntp.tn.icm.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


public class StudentVO {

    @NotNull(message = "Please provide first name")
    private String firstName;
    @NotNull(message = "Please provide last name")
    private String lastName;
    @NotNull
    @Min(value = 18, message = "Age should be equal or more than 18")
    private String age;
    @NotNull(message = "Please provide gender")
    private String gender;
    @NotNull(message = "Please provide classroom number")
    private int classroomNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getClassroomNumber() {
        return classroomNumber;
    }

    public void setClassroomNumber(int classroomNumber) {
        this.classroomNumber = classroomNumber;
    }
}
