package sg.gov.ntp.tn.icm.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sg.gov.ntp.tn.icm.dao.StudentRepository;
import sg.gov.ntp.tn.icm.model.Classroom;
import sg.gov.ntp.tn.icm.model.Student;
import sg.gov.ntp.tn.icm.model.StudentVO;

import javax.annotation.Resource;


@Service
public class StudentService implements StudentFacade {

	@Resource
	private StudentRepository studentRepository;

	@Override
	@Transactional
	public void saveStudent(StudentVO studentVo) {

		Classroom classroom = new Classroom();
		Student student = new Student();

		student.setFname(studentVo.getFirstName());
		student.setLname(studentVo.getLastName());
		student.setAge(Integer.valueOf(studentVo.getAge()));
		student.setGender(studentVo.getGender());
		student.setClassroom(classroom);
		classroom.setClassroomNo(studentVo.getClassroomNumber());
		classroom.setStudent(student);
		
		studentRepository.save(student);
	}

	@Override
	@Transactional
	public Student getStudent(String id) {
		return studentRepository.findById(id);
	}
}
