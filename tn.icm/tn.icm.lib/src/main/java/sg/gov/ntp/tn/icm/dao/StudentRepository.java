package sg.gov.ntp.tn.icm.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sg.gov.ntp.tn.icm.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

    public Student save(Student student);
    public Student findById(String id);

}
