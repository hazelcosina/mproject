package sg.gov.ntp.tn.icm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import sg.gov.ntp.tn.icm.model.Student;
import sg.gov.ntp.tn.icm.model.StudentVO;
import sg.gov.ntp.tn.icm.service.StudentFacade;

import javax.validation.Valid;

/**
 * 
 * @author hazel.mae.t.cosina
 * 
 */
@Controller
public class StudentController {

	@Autowired
	private StudentFacade studentFacade;

	/**
	 * This method saves student
	 * 
	 * @param student
	 * @return
	 */
	@RequestMapping(value = "/ntp/save", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public ResponseEntity saveStudent(@Valid @RequestBody StudentVO student) {
		studentFacade.saveStudent(student);
		return new ResponseEntity<String>("Customer information is saved.", HttpStatus.CREATED);
	}

	/**
	 * This method retrieves student using id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/ntp/student/{id}", method = RequestMethod.GET, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Student> getStudent(@PathVariable String id) {
		Student student = studentFacade.getStudent(id);
		return new ResponseEntity<Student>(student, HttpStatus.OK);
	}
}
