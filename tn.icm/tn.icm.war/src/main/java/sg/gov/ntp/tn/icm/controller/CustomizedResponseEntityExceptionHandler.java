package sg.gov.ntp.tn.icm.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@Controller
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
    	BindingResult bindingResult = ex.getBindingResult();
    	FieldError field = bindingResult.getFieldError();
    	String validationError = field.getDefaultMessage();
    	
        ErrorDetails errorDetails = new ErrorDetails(new Date(), "Customer information is not saved. ", validationError);
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
