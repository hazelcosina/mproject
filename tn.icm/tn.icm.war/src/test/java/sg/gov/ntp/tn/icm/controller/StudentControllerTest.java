package sg.gov.ntp.tn.icm.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import sg.gov.ntp.tn.icm.model.Classroom;
import sg.gov.ntp.tn.icm.model.Student;
import sg.gov.ntp.tn.icm.model.StudentVO;
import sg.gov.ntp.tn.icm.service.StudentFacade;

import com.fasterxml.jackson.databind.ObjectMapper;

public class StudentControllerTest {

	private MockMvc mockMvc;
	private ObjectMapper objectMapper;
	private Student student;
	private StudentVO studentVo;
	private Classroom classroom;

	@Mock
	private StudentFacade facadeMock;

	@InjectMocks
	private StudentController studentController;

	/**
	 * This method will be executed before each test
	 */
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(studentController)
				.setControllerAdvice(new CustomizedResponseEntityExceptionHandler()).build();
		objectMapper = new ObjectMapper();
		student = new Student();
		studentVo = new StudentVO();
		classroom = new Classroom();
	}
	
	/**
	 * Test Case
	 * @throws Exception
	 */
	@Test
	public void testGetStudent_validId() throws Exception{
		
		/**
		 * Test Condition
		 * Facade is only a mock and  can't return real value from db, hence,
		 * we set the expected value as variable student. 
		 */
		student.setId("267");
		student.setFname("jose");
		student.setLname("rizal");
		student.setGender("M");
		student.setAge(19);
		classroom.setClassroomId(1);
		classroom.setClassroomNo(1);
		student.setClassroom(classroom);

	    Mockito.when(facadeMock.getStudent("267")).thenReturn(student);
	    
	    /**
	     * Expected Results
	     * MockMvc is a running instance of StudentController with a mocked dependency 'facade'
	     * The andExpect() asserts if returned value matches our expected result.
	     */
	    mockMvc.perform(get("/ntp/student/267").contentType(MediaType.APPLICATION_JSON))
	    	.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.fname", is("jose")))
            .andExpect(jsonPath("$.lname", is("rizal")))
            .andExpect(jsonPath("$.gender", is("M")))
            .andExpect(jsonPath("$.age", is(19)));
		
		}
	
	@Test
	public void testSaveStudent_allFieldsValid() throws Exception{
		/**
		 * Test Condition
		 * When and then is not applicable here since 'facade' is void, no return value.
		 * Hence, no expected result
		 */
		studentVo.setFirstName("jose");
		studentVo.setLastName("rizal");
		studentVo.setGender("M");
		studentVo.setAge("50");
		studentVo.setClassroomNumber(1);
				
		String jsonRequest = objectMapper.writeValueAsString(studentVo);
		
		/**
		 * Expected Results
		 */
		mockMvc.perform(post("/ntp/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(content().string("Customer information is saved."));
	}
	
	@Test
	public void testSaveStudent_nullFirstName() throws Exception{
		studentVo.setFirstName(null);
		studentVo.setLastName("rizal");
		studentVo.setGender("M");
		studentVo.setAge("18");
		studentVo.setClassroomNumber(1);
				
		String jsonRequest = objectMapper.writeValueAsString(studentVo);
		
		mockMvc.perform(post("/ntp/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("Customer information is not saved. ")))
				.andExpect(jsonPath("$.description", is("First name cannot be null.")));
	}
	
	@Test
	public void testSaveStudent_emptyFirstName() throws Exception{
		studentVo.setFirstName("");
		studentVo.setLastName("rizal");
		studentVo.setGender("M");
		studentVo.setAge("18");
		studentVo.setClassroomNumber(1);
				
		String jsonRequest = objectMapper.writeValueAsString(studentVo);
		
		mockMvc.perform(post("/ntp/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("Customer information is not saved. ")))
				.andExpect(jsonPath("$.description", is("First name cannot be empty.")));
	}
	
	@Test
	public void testSaveStudent_invalidAge() throws Exception{
		
		studentVo.setFirstName("jose");
		studentVo.setLastName("rizal");
		studentVo.setGender("M");
		studentVo.setAge("1");
		studentVo.setClassroomNumber(1);
				
		String jsonRequest = objectMapper.writeValueAsString(studentVo);
		
		mockMvc.perform(post("/ntp/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(jsonPath("$.message", is("Customer information is not saved. ")))
				.andExpect(jsonPath("$.description", is("Age should be equal or more than 18")));
				
	}
}