-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: studentdb
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classroom`
--

DROP TABLE IF EXISTS `classroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `classroom` (
  `classroom_id` int(19) NOT NULL AUTO_INCREMENT,
  `classroom_no` int(19) NOT NULL,
  `student_id` int(19) NOT NULL,
  PRIMARY KEY (`classroom_id`),
  KEY `FK_STUDENT_ID` (`student_id`),
  CONSTRAINT `fk_class` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classroom`
--

LOCK TABLES `classroom` WRITE;
/*!40000 ALTER TABLE `classroom` DISABLE KEYS */;
INSERT INTO `classroom` VALUES (1,11,34),(2,1,35),(3,15,36),(4,15,37),(5,15,38),(6,3,39),(7,3,40),(8,3,41),(9,3,42),(10,3,43),(11,3,44),(12,3,45),(13,3,46),(14,3,47),(15,3,48),(16,3,49),(17,3,50),(18,3,51),(19,3,52),(20,3,53),(21,3,54),(22,3,55),(23,3,56),(24,3,57),(25,3,58),(26,3,59),(27,3,60),(28,3,61),(29,3,62),(30,3,63),(31,3,64),(32,3,65),(33,3,66),(34,3,67),(35,3,68),(36,3,69),(37,3,70),(38,3,71),(39,3,72),(40,3,73),(41,3,74),(42,3,75),(43,3,76),(44,3,77),(45,3,78),(46,3,79),(47,3,80),(48,3,81),(49,3,82),(50,3,83),(51,3,84),(52,3,85),(53,3,86),(54,3,87),(55,3,88),(56,3,89),(57,3,90),(58,3,91),(59,3,92),(60,3,93);
/*!40000 ALTER TABLE `classroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `student_id` int(19) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `age` int(50) NOT NULL,
  `gender` varchar(2) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (2,'hazel','cosina',23,'f'),(3,'hazel','cosina',23,'f'),(4,'hazel','cosina',23,'f'),(5,'hazel','cosina',23,'f'),(6,'hazel','cosina',23,'f'),(7,'hazel','cosina',23,'f'),(8,'hazel','cosina',23,'f'),(9,'hazel','cosina',23,'f'),(10,'hazel','cosina',23,'f'),(11,'hazel','cosina',23,'f'),(12,'juan','dela cruz',1,'m'),(13,'kathy','gonzales',29,'f'),(14,'kathy','gonzales',29,'f'),(15,'kathy2','gonzales',29,'f'),(16,'juan','dela cruz',1,'m'),(17,'juan','dela cruz',4,'m'),(18,'juan','dela cruz',9,'m'),(19,'hahahha','dela cruz',4,'m'),(20,'anne','curtis',4,'f'),(21,'anne','curtis',4,'f'),(22,'anne j','curtis',4,'f'),(23,'jasmine','curtis',4,'f'),(24,'jasmine','curtis',19,'f'),(25,'hazel','curtis',19,'f'),(26,'kathy','estorque',20,'f'),(28,'kathy8','estorque',20,'f'),(29,'hazel','csn',20,'f'),(30,'hazel','csn',20,'f'),(31,'taylor','swift',30,'f'),(33,'katy','perry',30,'f'),(34,'rih','hanna',22,'f'),(35,'rih','hanna',22,'f'),(36,'john','mayer',22,'m'),(37,'bruno','mars',19,'m'),(38,'bruno','mars',19,'m'),(39,'jose','Rizal',22,'f'),(40,'jose','Rizal',22,'f'),(41,'jose','Rizal',22,'f'),(42,'jose','Rizal',22,'f'),(43,'Jose','Rizal',22,'f'),(44,'Jose','Rizal',22,'f'),(45,'Jose','Rizal',22,'f'),(46,'Jose','Rizal',22,'f'),(47,'Jose','Rizal',22,'f'),(48,'Jose','Rizal',22,'f'),(49,'Jose','Rizal',22,'f'),(50,'Jose','Rizal',22,'f'),(51,'Jose','Rizal',22,'f'),(52,'Jose','Rizal',22,'f'),(53,'Jose','Rizal',22,'f'),(54,'Jose','Rizal',22,'f'),(55,'Jose','Rizal',22,'f'),(56,'Jose','Rizal',22,'f'),(57,'Jose','Rizal',22,'f'),(58,'Jose','Rizal',22,'f'),(59,'Jose','Rizal',22,'f'),(60,'Jose','Rizal',22,'f'),(61,'Jose','Rizal',22,'f'),(62,'Jose','Rizal',22,'f'),(63,'Jose','Rizal',22,'f'),(64,'Jose','Rizal',22,'f'),(65,'Jose','Rizal',22,'f'),(66,'Jose','Rizal',22,'f'),(67,'Jose','Rizal',22,'f'),(68,'Jose','Rizal',22,'f'),(69,'Jose','Rizal',22,'f'),(70,'Jose','Rizal',22,'f'),(71,'Jose','Rizal',22,'f'),(72,'Jose','Rizal',22,'f'),(73,'Jose','Rizal',22,'f'),(74,'Jose','Rizal',22,'f'),(75,'Jose','Rizal',22,'f'),(76,'Jose','Rizal',22,'f'),(77,'Jose','Rizal',22,'f'),(78,'Jose','Rizal',22,'f'),(79,'Jose','Rizal',22,'f'),(80,'Jose','Rizal',22,'f'),(81,'Jose','Rizal',22,'f'),(82,'Jose','Rizal',22,'f'),(83,'Jose','Rizal',22,'f'),(84,'Jose','Rizal',22,'f'),(85,'Jose','Rizal',22,'f'),(86,'Jose','Rizal',22,'f'),(87,'Jose','Rizal',22,'f'),(88,'Jose','Rizal',22,'f'),(89,'Jose','Rizal',22,'f'),(90,'Jose','Rizal',22,'f'),(91,'Jose','Rizal',22,'f'),(92,'Jose','Rizal',22,'f'),(93,'Jose','Rizal',22,'f');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-23 20:01:21
